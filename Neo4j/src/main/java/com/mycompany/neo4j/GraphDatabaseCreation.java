/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.neo4j;

import java.io.File;
import java.io.FileInputStream;
import org.apache.commons.io.FilenameUtils;
import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.TransactionWork;
import static org.neo4j.driver.v1.Values.parameters;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.*;
import com.github.javaparser.ast.body.FieldDeclaration;
import java.io.BufferedReader;
import java.io.FileReader;
/**
 *
 * @author ricardocarvalho
 */
public class GraphDatabaseCreation {
    private static final String mainDirectory = "/Users/ricardocarvalho/Downloads/intelligentdatabroker-master 5/";
    static Driver driver;
    
    public static void main(String[] args) throws Exception{
        //dados do servidor Neo4j
        GraphDatabaseCreation graph = new GraphDatabaseCreation("bolt://localhost:7687", "neo4j", "graph" ) ;
        
            graph.CreateDatabase(mainDirectory);
            
            
            close();
       
    }
    
    
    
    // Autenticação ao servidor do Neo4j
    public GraphDatabaseCreation(String uri, String user, String password){
        driver = GraphDatabase.driver( uri, AuthTokens.basic( user, password ) );
    }
    
    // Metodo para retirar os metodos do ficheiros Java
   private static class MethodVisitor1 extends VoidVisitorAdapter {
       public void visit(MethodDeclaration n, Object arg) {
            
           System.out.println(n.getName());
       
  }
     
      
    }
      //metodo para terminar o programa
    public static void close() throws Exception
    {
        driver.close();
    }
    
    
    // Metodo para retirar o nomes das variaveis e os seus tipos
    private static class ClassVisitor1 extends VoidVisitorAdapter<Void>{
       @Override
       public void visit(ClassOrInterfaceDeclaration n, Void arg){
           for(final FieldDeclaration field : n.getFields()){
            NodeList<VariableDeclarator> s = field.getVariables();
           
    System.out.print(s.get(0).getName() + ": "+ s.get(0).getType() + "; ");
   

           }
       }
       
   }
    
    // Metodo de criação da BD de Grafos
    public static void CreateDatabase(String directoryName) throws Exception{
       
    File directory = new File(directoryName);
    
   

    // vasculha dentro dos ficheiros .java e .drl
    File[] fList = directory.listFiles();
    if(fList != null)
        for (File file : fList) {      
            if (file.isFile() && FilenameUtils.getExtension(file.getAbsolutePath()).equals("java") || FilenameUtils.getExtension(file.getAbsolutePath()).equals("drl")) {
                 System.out.println();
                    System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *") ;
                   
                    if(FilenameUtils.getExtension(file.getAbsolutePath()).equals("java")){
                        
            
                try(Session session = driver.session()){
                    FileInputStream in = new FileInputStream(file.getAbsolutePath());
                                          
                            CompilationUnit cd = StaticJavaParser.parse(in);
                    String greeting = session.writeTransaction( new TransactionWork<String>()
            {
                   
                           // Metodo de criação dos nos no Neo4j a partir dos ficheiros .java
                        @Override
                        public String execute(org.neo4j.driver.v1.Transaction t) {
                           
                            String parentPath = file.getAbsolutePath().split(mainDirectory)[1].split("/")[0];
                            
                            
                    StatementResult result = t.run("CREATE (" + file.getName().split(".java")[0]+ ":" + parentPath +")" +
                                                     "SET "+ file.getName().split(".java")[0]+".message = $message " +       
                                                     "RETURN "+file.getName().split(".java")[0]+".message + ', from node ' + id("+file.getName().split(".java")[0]+")",
                             parameters( "message", file.getName().split(".java")[0]));
                            
                            return result.single().get( 0 ).asString();
                            
                        }
            });
                         //mostrar para o utilizador todas as variaveis e metodos do ficheiro no qual se encontra           
                        System.out.println( greeting );
                        System.out.println("Variaveis: ");
                        new ClassVisitor1().visit(cd, null);
                     System.out.println();
                     System.out.println("Métodos: ");
                     new MethodVisitor1().visit(cd, null);
                     System.out.println();
                    
                    
                            } 
                    }else{
                        try(Session session = driver.session()){
                    
                    String greeting = session.writeTransaction( new TransactionWork<String>()
            {
                    // Metodo de criação dos nos no Neo4j a partir dos ficheiros .
                        @Override
                        public String execute(org.neo4j.driver.v1.Transaction t) {
                           
                            String parentPath = file.getAbsolutePath().split(mainDirectory)[1].split("/")[0];
                            
                    StatementResult result = t.run("CREATE (" + file.getName().split(".drl")[0]+ ":" + parentPath +")" +
                                                     "SET "+file.getName().split(".drl")[0]+".message = $message " +      
                                                     "RETURN "+file.getName().split(".drl")[0]+".message + ', from node ' + id("+file.getName().split(".drl")[0]+")",
                             parameters( "message", file.getName().split(".drl")[0]));
                            
                            return result.single().get( 0 ).asString();
                            
                        }
                        
            });
                                     
                        System.out.println( greeting );
                    } 
                        
                        //codigo para retirar toda a informação proveniente de ficheiro Drools
                        BufferedReader br = new BufferedReader(new FileReader(file));
                        boolean flag = false;
                            try {
                                  String line;
                                     while ((line = br.readLine()) != null) {
                                         if(line.startsWith("when")){
                                             
                                                        flag = true;
                                            }
                                                if(flag){
                                                    String line1 = br.readLine();
                                                String[] parts = line1.split(": ");
                                                flag = false;
                                                System.out.println("Message(s):");
                                                System.out.println(parts[1]);
                                                
                                                }
                                         if(line.startsWith("then")){
                                             flag = true;
                                         }
                                                if(flag){
                                                System.out.println("Trigger(s):");
                                                while(!((line=br.readLine()).startsWith("end")))
                                                System.out.println(line);
                                                flag = false;
                                                }
                                         if(line.startsWith("rule")){
                                                String[] parts = line.split("rule \"");
                                                String[] f = parts[1].split("\"");
                                                System.out.println("Rule name:");
                                                System.out.println(f[0]);
                                                
                                            } 
                                         }
                                        } finally {
                                            br.close();
                                            }
                                            }
                        
                 
                  
        }else{
            
        CreateDatabase(file.getAbsolutePath());
        
        
        }
            
    }
    
}
   
   }